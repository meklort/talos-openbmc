FILESEXTRAPATHS_prepend := "${THISDIR}/obmc-targets:"
SRC_URI += "file://obmc-host-ipl-complete@.target"
SYSTEMD_SERVICE_${PN} += "obmc-host-ipl-complete@.target"

do_install_append() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/obmc-host-ipl-complete@.target ${D}${systemd_system_unitdir}
}
