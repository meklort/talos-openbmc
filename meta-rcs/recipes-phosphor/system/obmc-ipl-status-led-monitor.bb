SUMMARY = "OpenBMC IPL status LED monitor"
DESCRIPTION = "OpenBMC IPL status LED monitor."
PR = "r1"

inherit skeleton-python
inherit obmc-phosphor-dbus-service

VIRTUAL-RUNTIME_skeleton_workbook ?= ""

RDEPENDS_${PN} += "\
        python-dbus \
        python-json \
        python-subprocess \
        python-pygobject \
        pyphosphor \
        pyphosphor-dbus \
        ${VIRTUAL-RUNTIME_skeleton_workbook} \
        obmc-ipl-status-observer \
        "

SKELETON_DIR = "pyiplledmonitor"

DBUS_SERVICE_${PN} += "org.openbmc.status.IPLLED.service"
