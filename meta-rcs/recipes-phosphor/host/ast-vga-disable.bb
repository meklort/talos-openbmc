SUMMARY = "Talos II VGA Disable"
DESCRIPTION = "Talos II power on VGA disable controller"
PR = "r0"

LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464 \
                    file://COPYING.LESSER;md5=3000208d539ec061b899bce1d9ce9404 \
                    "

inherit obmc-phosphor-systemd

PROVIDES += 'virtual/talos-ast-vga-disable'
RPROVIDES_${PN} += 'virtual-talos-ast-vga-disable'

RDEPENDS_${PN} += "bash"

S = "${WORKDIR}"
SRC_URI += "file://ast-vga-disable-check"
SRC_URI += "file://COPYING"
SRC_URI += "file://COPYING.LESSER"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/ast-vga-disable-check ${D}${bindir}/ast-vga-disable-check
}

TMPL = "ast_vga_disable@.service"
INSTFMT = "ast_vga_disable@{0}.service"
TGTFMT = "obmc-chassis-poweron@{0}.target"
FMT = "../${TMPL}:${TGTFMT}.requires/${INSTFMT}"

SYSTEMD_SERVICE_${PN} += "${TMPL}"
SYSTEMD_LINK_${PN} += "${@compose_list(d, 'FMT', 'OBMC_CHASSIS_INSTANCES')}"
