SUMMARY = "OpenBMC hardware PWM beep"
DESCRIPTION = "A sample implementation for hardware beep on a PWM."
PR = "r1"

inherit skeleton-gdbus
inherit pkgconfig

LICENSE = "GPLv3"
SKELETON_DIR = "hardbeep"
