SRCREV ?= "22ce0ab4009b952904f78b72d56b6c2b7e635c88"
SKELETON_URI ?= "git://scm.raptorcs.com/scm/git/talos-skeleton;protocol=https;branch=04-16-2019"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=e3fc50a88d0a364313df4b21ef20c29e"
