SUMMARY = "Phosphor OpenBMC pre-init scripts"
DESCRIPTION = "Phosphor OpenBMC filesystem mount reference implementation."
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${PHOSPHORBASE}/COPYING.apache-2.0;md5=34400b68072d710fecd0a2940a0d1658"

PR = "r1"

INSANE_SKIP_${PN} += "already-stripped"

DEPENDS += "${@d.getVar('PREFERRED_PROVIDER_u-boot', True) or 'u-boot'}"
DEPENDS += "${@d.getVar('PREFERRED_PROVIDER_u-boot-fw-utils', True) or 'u-boot-fw-utils'}"

RDEPENDS_${PN} += "${VIRTUAL-RUNTIME_base-utils}"

S = "${WORKDIR}"
SRC_URI += "file://obmc-init.sh"
SRC_URI += "file://obmc-shutdown.sh"
SRC_URI += "file://obmc-update.sh"
SRC_URI += "file://whitelist"

do_install() {
	for f in init-download-url init-options
	do
		if test -e $f
		then
			install -m 0755 ${WORKDIR}/$f ${D}/$f
		fi
	done
        install -m 0755 ${WORKDIR}/obmc-init.sh ${D}/init
        install -m 0755 ${WORKDIR}/obmc-shutdown.sh ${D}/shutdown
        install -m 0755 ${WORKDIR}/obmc-update.sh ${D}/update
        install -m 0644 ${WORKDIR}/whitelist ${D}/whitelist
        install -d ${D}/dev
        mknod -m 622 ${D}/dev/console c 5 1

        install -d ${D}/etc
        install -m 0644 ${STAGING_DIR}/fw_env.config ${D}/etc/fw_env.config
        install -d ${D}/bin
        install -m 0755 ${STAGING_DIR}/bin/fw_printenv ${D}/bin/fw_printenv
        install -m 0755 ${STAGING_DIR}/bin/bbe ${D}/bin/bbe
}

FILES_${PN} += " /init /shutdown /update /whitelist /dev "
FILES_${PN} += " /init-options /init-download-url "
FILES_${PN} += " /bin/fw_printenv /bin/bbe "
