SUMMARY     = "PowerPC FSI Debugger"
DESCRIPTION = "pdbg allows JTAG-like debugging of the host POWER processors"
LICENSE     = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=3b83ef96387f14655fc854ddc3c6bd57"

PV = "2.0+git${SRCPV}"

SRC_URI += "git://scm.raptorcs.com/scm/git/pdbg;branch=master;protocol=https;branch=04-16-2019"
SRCREV = "a1f574fe60cf3e299f22bc39f4de40bc7ea0d87c"

DEPENDS += "dtc-native"

S = "${WORKDIR}/git"

inherit autotools
